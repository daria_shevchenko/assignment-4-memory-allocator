#define _GNU_SOURCE

#include <assert.h>
#include <stdio.h>
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"

#define HEAP_SIZE 4096
#define HEAP_START ((void*)0x04040000)
#define ALLOCATED 2
#define NUMBER_SECOND_TEST 64
#define NUMBER_THIRD_TEST 132
#define NUMBER_FOURTH_TEST 30
#define NUMBER_FIFTH_TEST 128

extern void debug(const char *fmt, ...);

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

//Обычное успешное выделение памяти.
void test1() {
    debug("Test №1\n");
    void *heap = heap_init(HEAP_SIZE);
    assert(heap);
    void *allocated = _malloc(ALLOCATED);
    assert(allocated != NULL);
    _free(allocated);
    heap_term();
    debug("Cool\n");
}

//Освобождение одного блока из нескольких выделенных.
void test2() {
    debug("Test №2\n");
    void *heap = heap_init(HEAP_SIZE);
    assert(heap);
    debug("Heap create, size %d\n", HEAP_SIZE);
    void *allocated_first = _malloc(NUMBER_SECOND_TEST);
    assert(allocated_first);
    void *allocated_second = _malloc(NUMBER_THIRD_TEST);
    assert(allocated_second);
    void *allocated_third = _malloc(NUMBER_FOURTH_TEST);
    assert(allocated_third);

    _free(allocated_second);
    debug("Heap: ");
    debug_heap(stdout, heap);

    _free(allocated_first);
    _free(allocated_third);
    heap_term();
    debug("Cool\n");
}

//Освобождение двух блоков из нескольких выделенных.
void test3() {
    debug("Test №3\n");
    void *heap = heap_init(HEAP_SIZE);
    assert(heap);
    debug("Heap create, size %d\n", HEAP_SIZE);
    void *allocated_first = _malloc(NUMBER_SECOND_TEST);
    void *allocated_second = _malloc(NUMBER_FIFTH_TEST);
    void *allocated_third = _malloc(NUMBER_THIRD_TEST);
    assert(allocated_first);
    assert(allocated_second);
    assert(allocated_third);
    debug("Done\n");

    debug("Delete 2 blocks\n");
    _free(allocated_third);
    _free(allocated_first);
    assert(allocated_second != NULL);
    debug("Heap: \n");
    debug_heap(stdout, heap);

    _free(allocated_second);
    heap_term();

}

//Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
void test4() {
    debug("Test №4\n");
    debug("Create region\n", 0);
    void *mmap_addr = map_pages(HEAP_START, 15, MAP_FIXED);
    assert(mmap_addr);
    debug("Alloc to occupied region\n", 0);
    void *alloc_first = _malloc(REGION_MIN_SIZE * 2);
    assert(mmap_addr != alloc_first);
    _free(alloc_first);
    heap_term();
    debug("Cool\n");
}

int main() {
    test1();
    debug("\n");
    test2();
    debug("\n");
    test3();
    debug("\n");
    test4();
    debug("\n");
    debug("Great\n");
}
